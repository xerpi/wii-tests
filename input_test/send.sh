#!/usr/bin/env bash

mkdir -p FAKEMOTE
cp ../fakemote/FAKEMOTE.app FAKEMOTE
cp input_test.dol FAKEMOTE/boot.dol
zip -r FAKEMOTE.zip FAKEMOTE
wiiload FAKEMOTE.zip
