#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <malloc.h>
#include <gccore.h>
#include <wiiuse/wpad.h>
#include <ogc/machine/processor.h>

static int run = 1;

static void button_pressed()
{
	run = 0;
}

int __IOS_LoadStartupIOS(void)
{
	/* Load IOS before C runtime to have MLOAD before CONF_Init() is called */
	IOS_ReloadIOS(252);
	usleep(500 * 1000);

	return 0;
}

int main(int argc, char **argv)
{

	void *xfb;
	GXRModeObj *rmode;

	SYS_SetResetCallback(button_pressed);
	SYS_SetPowerCallback(button_pressed);

	VIDEO_Init();
	WPAD_Init();
	WPAD_SetDataFormat(WPAD_CHAN_ALL, WPAD_FMT_BTNS_ACC_IR);

	printf("\x1b[2;0H");

	rmode = VIDEO_GetPreferredMode(NULL);
	xfb = MEM_K0_TO_K1(SYS_AllocateFramebuffer(rmode));
	console_init(xfb, 0, 0, rmode->fbWidth, rmode->xfbHeight-2, rmode->fbWidth * VI_DISPLAY_PIX_SZ);
	VIDEO_Configure(rmode);
	VIDEO_SetNextFramebuffer(xfb);
	VIDEO_SetBlack(FALSE);
	VIDEO_Flush();
	VIDEO_WaitVSync();
	if (rmode->viTVMode&VI_NON_INTERLACE)
		VIDEO_WaitVSync();

	printf("Entering main loop\n");

	while (run) {
		WPAD_ScanPads();
		u32 pressed0 = WPAD_ButtonsHeld(0);
		u32 pressed1 = WPAD_ButtonsHeld(1);
		if ((pressed0 | pressed1) & WPAD_BUTTON_HOME)
			run = 0;
		if (pressed0)
			printf("Pressed[0]: 0x%08x\n", pressed0);
		if (pressed1)
			printf("Pressed[1]: 0x%08x\n", pressed1);

		u32 exp_type;
		WPAD_Probe(0, &exp_type);
		WPADData *data = WPAD_Data(0);
		//printf("present: %d\n", data->data_present);
		if (exp_type == WPAD_EXP_NUNCHUK) {
			//printf("bat: 0x%x\n", WPAD_BatteryLevel(0));
			printf("x: %03d, y: %03d\n", data->exp.nunchuk.js.pos.x, data->exp.nunchuk.js.pos.y);
		}

		VIDEO_WaitVSync();
	}

	printf("\n\nExiting...\n");

	return 0;
}
